import { useEffect, useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./styles/App.css";
import FlipClockCountdown from "@leenguyen/react-flip-clock-countdown";
function App() {
  const [date, setDate] = useState(new Date("2025-06-08T22:40").getTime());
  // const [hours, setHours] = useState(0);
  // const [minutes, setMinutes] = useState(0);
  // const [seconds, setSeconds] = useState(0);
  // const [day, setDay] = useState(0);
  const now = new Date().getTime();
  const handleChange = (e) => {
    setDate(new Date(e.target.value).getTime());
  };
  const distance = date - now;
  // useEffect(() => {
  //   setDate(new Date("2023-12-09T22:40").getTime());
  //   const day = Math.abs(Math.floor(distance / (1000 * 60 * 60 * 24)));
  //   const hours = Math.abs(
  //     Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
  //   );
  //   const minutes = Math.abs(
  //     Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
  //   );
  //   const seconds = Math.abs(Math.floor((distance % (1000 * 60)) / 1000));
  //   const time = setInterval(() => {
  //     setDay(day);
  //     setHours(hours);
  //     setMinutes(minutes);
  //     setSeconds(seconds);
  //   }, 1000);
  //   return () => clearInterval(time);
  // }, [seconds, date]);
  const check = (num) => {
    if (num < 10) {
      return "0" + num;
    }
    return num;
  };
  return (
    <div className="App">
      <div className="inputs-date">
        <p>Input future date</p>
        <input type="datetime-local" onChange={handleChange} />
      </div>
      <div className="counter">
        <FlipClockCountdown to={now + distance} />

      </div>
      <div className="icons">
        <a href="https://gitlab.com/group_tarashev" target="_blank">
          <img src="../src/assets/images/git.svg" alt="" />
        </a>
        <a
          href="https://www.linkedin.com/in/iliyan-tarashev-a7063b122"
          target="_blank"
        >
          <img src="../src/assets/images/linkedin.svg" alt="" />
        </a>
      </div>
      <img
        className="image"
        src="../src/assets/images/pattern-hills.svg"
        alt=""
      />
    </div>
  );
}

export default App;
